<?php

namespace app\Services;

use PDO, PDOException;

class DBService {
  
    public $connection;
    public $pdoError;

    public function __construct($host, $port, $user, $password, $dbname)
    {
        $this->connection = $this->pdoConnect($host, $port, $user, $password, $dbname);
    }

    public function pdoConnect($host, $port, $user, $password, $dbname)
    {
        $connection = false;

        try{
            $connection = new PDO("mysql:host={$host};port={$port};dbname={$dbname}", $user, $password);
//            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e){
          $this->pdoError = $e;
          $connection = false;
        }

        return $connection;
    }

    public function createVKOtpRow($userId, $code, $vkId){
        return $this->connection->query("INSERT INTO vk_otp(user_id, linked_vk, code, codeall) VALUES('{$userId}', '{$vkId}', '{$code}', '0')");
    }

    public function giveMoney($login, $amount){
        return $this->connection->query("UPDATE dle_users SET money = money + '{$amount}' WHERE name = '{$login}'");
    }

    public function updateOtpCode($userId, $code){
        return $this->connection->query("UPDATE vk_otp SET code = '{$code}' WHERE user_id = '$userId' ");
    }

    public function getVKUsers()
    {
        $getVKUsersQuery = $this->connection->prepare("SELECT `linked_vk` FROM `vk_otp`");
        $getVKUsersQuery->execute();
        return $getVKUsersQuery->fetchAll(PDO::FETCH_ASSOC);    

    }

    public function getUserId($username){
          $dUserQuery = $this->connection->query("SELECT * FROM dle_users WHERE name = '{$username}'")->fetch();
          return $dUserQuery['user_id'];
    }

    public function getUsername($userId){
        $dUserQuery = $this->connection->query("SELECT * FROM dle_users WHERE user_id = '{$userId}'")->fetch();
        return $dUserQuery['name'];
    }

    public function getUserInfo($userId){
        $dUserQuery = $this->connection->query("SELECT * FROM dle_users WHERE user_id = '{$userId}'")->fetch();
        $otpUserQuery = $this->connection->query("SELECT * FROM vk_otp WHERE user_id = '{$userId}'")->fetch();
        $userData = [
            'id' => $userId,
            'login' => $dUserQuery['name'],
            'balance' => $dUserQuery['money'],
            'isVKLinked' => intval($dUserQuery['vk']),
            'isTrueVKLinked' => $otpUserQuery['user_id'] === $userId ? 1 : 0,
            'code' => intval($otpUserQuery['code'])
        ];
        return $userData;
    }
	
	public function checkVkIsLinked($vkId){
		$query = $this->connection->query("SELECT * FROM vk_otp WHERE linked_vk = '{$vkId}'")->fetchAll();
		return count($query) >= 1 ? true : false;
	}

    public function linkVK($userId){
        $dUserQuery = $this->connection->query("UPDATE dle_users SET vk = '1' WHERE user_id = '{$userId}'");
        $otpQuery = $this->connection->query("UPDATE vk_otp SET code = '-1' WHERE user_id = '{$userId}'");
        return true;
    }

    public function unlinkVk($userId, $isOtpRowExists): bool
    {
          $dQuery = $this->connection->query("UPDATE dle_users SET vk = '0' WHERE user_id = '{$userId}'");

          if($isOtpRowExists){
              $otpRemoveQuery = $this->connection->query("DELETE FROM vk_otp WHERE user_id = '{$userId}'");
          }

          return true;
    }
  
  public function try(){
    if(!$this->connection) {
      exit("{$this->pdoError}");
    } else {
      exit("OK");
    }
  }
  
}

