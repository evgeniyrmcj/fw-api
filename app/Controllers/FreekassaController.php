<?php

namespace app\Controllers;

require __DIR__."/../configs/freekassa.php";
require __DIR__."/../configs/db.php";

use app\Services\DBService;


class FreekassaController
{

    public $merchant_id;
    public $secret;
    public $dbService;
    private $multiplier;

    public function __construct(){
        $this->dbService = new DBService(DB_DATA['host'], DB_DATA['port'], DB_DATA['user'], DB_DATA['pass'], DB_DATA['db']);
        $this->merchant_id = FK_CONFIG['merchant_id'];
        $this->secret = FK_CONFIG['secret_first'];
        $this->multiplier = FK_CONFIG['payment_multiplier'];
    }

    public function handlePayment($dataModel){
        //check sign
        $ourSign = $this->createSign($this->merchant_id, $this->secret, $dataModel['amount'], $dataModel['login'], true);
        if($ourSign != $dataModel['sign']){
           return false;
        }

        // GIVE THE MONEY

        $this->dbService->giveMoney($dataModel['login'], $dataModel['amount'] * $this->multiplier);

        return true;
    }

    public function createPaymentLink($amount, $userId){
        $login = $this->dbService->getUserInfo($userId)['login'];
        $amountInt = intval($amount);
        $sign = $this->createSign($this->merchant_id, $this->secret, $amountInt, $login);
        return "https://pay.freekassa.ru/?currency=RUB&m=$this->merchant_id&oa=$amountInt&o=$login&s=$sign";
    }

    public function createSign($merchantId, $secret, $amount, $login, $forHandling = false){
        return $forHandling ? md5("$merchantId:$amount:$secret:$login") : md5("$merchantId:$amount:$secret:RUB:$login")  ;
    }
}