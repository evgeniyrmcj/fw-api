<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace app\Controllers;

require __DIR__."/../configs/db.php";
require __DIR__."/../configs/vk.php";

use app\Services\DBService;
use app\Helpers\ResponseHelper;
use VK\Client\VKApiClient;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

/**
 * Description of VKAccountController
 *
 * @author Evgeniy Goryunov
 */
class VKAccountController {
    
    private $dbService, $responseHelper, $vkApiClient;

    public function __construct(){
        $this->dbService = new DBService(DB_DATA['host'], DB_DATA['port'], DB_DATA['user'], DB_DATA['pass'], DB_DATA['db']);
        $this->responseHelper = new ResponseHelper();
        $this->vkApiClient = new VKApiClient();
        }

    /**
     * @throws \Exception
     */
    public function generateLinkCode($username, $vkId){
        $userId = $this->dbService->getUserId($username);
        $userInfo = $this->dbService->getUserInfo($userId);
        $code = random_int(100000, 999999);
		
		if($this->dbService->checkVkIsLinked($vkId)){
			$this->sendMessage("Данный аккаунт ВКонтакте уже привязан к игровому аккаунту.", $vkId);
			$this->responseHelper->plainText(200, 'OK');
		}

        if($userInfo['isVKLinked'] && $userInfo['isTrueVKLinked']){
            $this->sendMessage("Игровой аккаунт {$username} уже привязан к аккаунту ВК.", $vkId);
            $this->responseHelper->plainText(200, 'OK');
        }

        if(!$userInfo['isVKLinked'] && $userInfo['isTrueVKLinked']){
            $this->dbService->updateOtpCode($userId, $code);
            $this->sendMessage("Код для привязки учетной записи {$username} к вашему аккаунту вк. Введите этот код: {$code} на сайте.", $vkId);
            $this->responseHelper->plainText(200, 'OK');
        }

        if($userInfo['isVKLinked'] && !$userInfo['isTrueVKLinked']){
            $this->dbService->unlinkVk($userId, false);
            $this->sendMessage("Произошла проблема при генерации кода. Попробуйте еще раз, теперь должно все сработать.", $vkId);
            $this->responseHelper->plainText(200, 'OK');
        }


        $this->dbService->createVKOtpRow($userId, $code, $vkId);
        $this->sendMessage("Код для привязки учетной записи {$username} к вашему аккаунту вк. Введите этот код: {$code} на сайте.", $vkId);
        $this->responseHelper->plainText(200, 'OK');


        $this->responseHelper->json(200, print_r($userInfo, false));

    }


    public function vkStartMailing($message)
    {
        $users = $this->dbService->getVKUsers();

        foreach($users as $user){
            $this->sendMessage($message, $user['linked_vk']);
        }
        $this->responseHelper->plainText(200, "ok");
        return true;
    }

    public function linkVKAccount($code, $userId){
        
        $userData = $this->dbService->getUserInfo($userId);
        
        if($userData['isVKLinked'] && $userData['isTrueVKLinked']){
            $this->responseHelper->json(200, "Account already linked!");
        }
        
        if(intval($userData['code']) !== intval($code)){
            $this->responseHelper->json(200, ['status' => 'wrong']);
        }

        $this->dbService->linkVK($userId);
        $this->responseHelper->json(200, ['status' => 'ok']);
        
    }

    /**

     */
    public function sendMessage(string $message, int $peerId) : bool
    {
        try{

            $this->vkApiClient->messages()->send(VK_API['token'], [
                'peer_id' => $peerId,
                'random_id' => mt_rand('10', '99999'),
                'message' => $message
            ]);

            $this->responseHelper->plainText(200, "ok");


        } catch (VKApiException|VKClientException $exception){
            $this->responseHelper->plainText(200, "ok");
            return false;
        }
    }

}
