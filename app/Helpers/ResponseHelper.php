<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */
namespace App\Helpers;
/**
 * Description of ResponseHelper
 *
 * @author Evgeniy Goryunov
 */
class ResponseHelper {
    
    public function __construct(){
        
    }
    
    public function json($statusCode, $message){
        http_response_code($statusCode);
        exit(json_encode($message));
    }

    public function plainText($statusCode, $message){
        http_response_code($statusCode);
        exit($message);
    }
}
