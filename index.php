<?php

require "./vendor/autoload.php";


$vkC = new app\Controllers\VKAccountController();
$fkC = new app\Controllers\FreekassaController();
$response = new app\Helpers\ResponseHelper();

if(isset($_GET['action'])){
    switch($_GET['action']){
        case "vkApi":
            $request = json_decode(file_get_contents("php://input"), true);
            $messageBody = explode(' ', $request['object']['body']);
            $commands = [

                [
                    'command' => ['привязать', '!привязать', 'привязка'],
                    'message' => 'Привязать аккаунт можно в Личном кабинете!'
                ],

                [
                    'command' => ['отвязать', '!отвязать', 'отвязка'],
                    'message' => 'Отвязать аккаунт можно в Личном кабинете!'
                ]
            ];
            switch(strtolower($messageBody[0])){
                case "!link":

                    if(strlen($messageBody[1]) < 3){
                        $vkC->sendMessage("Неправильно указан ник. Минимальная длина ника: 3 символа.", $request['object']['user_id']);
                        $response->plainText(200, 'ok');
                    }

                    $vkC->generateLinkCode($messageBody[1], $request['object']['user_id']);
                    break;
                case "начать":
                    $vkC->sendMessage('Добро пожаловать в группу проекта FW', $request['object']['user_id']);
                    break;
                default:
                    break;
            }

            foreach($commands as $command){
                foreach($command['command'] as $cmd){
                    if(in_array($cmd, $messageBody)){
                        $vkC->sendMessage($command['message'], $request['object']['user_id']);
                        $response->plainText(200, 'ok');
                    }
                }
            }


            $response->plainText(200, 'ok');

            break;
        case "vkMailing":
            
            $message = $_POST['message'];

            if(strlen($message) <= 1) $response->json(419, ['status' => 'invalid message']);

            if($vkC->vkStartMailing($message)){
                $response->json(200, ['status' => 'Mailing is running.']);
            } else {
                $response->json(400, ['status' => 'Something wrong']);
            }

            break;
		case "fk":
		    if(!isset($_GET['method'])) $response->json(419, ['freekassa api method not defined']);

                switch($_GET['method']){
                    case "handlePayment":
                        if(!isset($_POST['MERCHANT_ID']) || !isset($_POST['AMOUNT']) || !isset($_POST['MERCHANT_ORDER_ID']) || !isset($_POST['SIGN'])){
                            $response->plainText(419, 'wrong request model!');
                        }

                        $dataModel = [
                            'merchant_id' => $_POST['MERCHANT_ID'],
                            'amount' => $_POST['AMOUNT'],
                            'login' => $_POST['MERCHANT_ORDER_ID'],
                            'sign' => $_POST['SIGN']
                        ];

                        $fkC->handlePayment($dataModel) ? $response->plainText(200, "YES") : $response->plainText(419, "ERR");
                        break;
                    case "createPaymentLink":
                        $response->plainText(200, $fkC->createPaymentLink($_GET['amount'], preg_replace('/([^\d])/','',$_GET['login'])));
                }
		break;
        case "linkAccount":
            if(!isset($_GET['code']) || !isset($_GET['userId'])){
                $response->json(419, ['status' => 'wrong request model']);
            }
            $vkC->linkVKAccount($_GET['code'], $_GET['userId']);
            break;
        default:
            $response->json(200, ['status' => 'ok', 'message' => 'FW API 1.1']);
            break;
    }
} else {
    $response->json(419, ['status' => 'error', 'message' => 'Api method not defined!']);
}



